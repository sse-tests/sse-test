# SSE-tests

Проект для ознакомления построения простых клиентов и поставщиков данных на базе SSE для OSGI на платформе karaf.

# Оглавление 

- [SSE-tests](#sse-tests)
- [Оглавление](#оглавление)
- [Требования к software](#требования-к-software)
- [Используемый стек технологий](#используемый-стек-технологий)
- [Сборка приложения](#сборка-приложения)
	- [Сборка с помощью PowerShell](#сборка-с-помощью-powershell)
	- [Команды для сборки приложения](#команды-для-сборки-приложения)
- [Требование к версии karaf](#требование-к-версии-karaf)
- [Предварительная установка на karaf](#предварительная-установка-на-karaf)
	- [Очистка karaf](#очистка-karaf)
	- [Установка зависимостей](#установка-зависимостей)
	- [Установка приложения](#установка-приложения)
- [Настройка логов](#настройка-логов)
- [Конфигурация SSE-клиента](#конфигурация-sse-клиента)

# Требования к software

- Java 8;
- Apache Maven 3.6.1.

# Используемый стек технологий

- Java SE;
- Apache Maven;
- Apache CXF;
- Apache OSGI;
- Apache Karaf.

# Сборка приложения

Используется сборщик apache maven версии 3.6.1. 

## Сборка с помощью PowerShell

Для сборки проекта через терминал maven необходимо прописать следующую команду:
```bash
& "%MAVEN_PATH%\mvn.cmd" clean install -f "%POM_PATH%\pom.xml"
```
Где:
- MAVEN_PATH - путь до мавена в файловой системе;
- POM_PATH - путь до pom.xml данного проекта в файловой системе.

## Команды для сборки приложения

- Очистка и удаление *\target*:

```bash
mvn clean
```

- Сборка артефактов:

```bash
mvn package
```

- Сборка артефактов и добавление в локальный репозиторий:

```bash
mvn install
```

- Очистка *\target*, сборка артефактов и добавление в локальный репозиторий:

```bash
mvn clean install
```

# Требование к версии karaf

Рекомендуемая версия karaf лежит в диапазоне [4.2,4.3).

# Предварительная установка на karaf

Для того, чтобы запустить приложение в среде исполнения *apache-karaf*, необходимо выполнить предварительную установку зависимостей.

## Очистка karaf

Если требуется очистить *cache* у *karaf*, то для этого необходимо:
- запустить *karaf.bat* с параметром *clean*;
- выйти из консоли с помощью комбинации *CTRL+D*

## Установка зависимостей

- Запускаем консоль *karaf* (karaf.bat);
- Выполняем следующие команды:
  - feature:install webconsole
  - feature:install aries-blueprint
  - feature:install http
  - feature:repo-add mvn:org.apache.cxf.karaf/apache-cxf/"[3.2.6,4)"/xml/features
  - feature:install cxf-jackson
  - feature:install cxf-sse
  - feature:install cxf-rs-description-swagger2
  - feature:install cxf-rs-description-openapi-v3
  - feature:install cxf-features-logging
  - bundle:install mvn:io.swagger/swagger-annotations/"[1.5,1.6)"
  - bundle:install mvn:io.swagger/swagger-models/"[1.5,1.6)"
  - bundle:install mvn:io.swagger/swagger-core/"[1.5,1.6)"
  - bundle:install mvn:io.swagger/swagger-jaxrs/"[1.5,1.6)"

## Установка приложения

Для установки приложения необходимо прописать следующую команду:

```bash
bundle:install mvn:ru.volodin.utils.sse/sse-test/
```

# Настройка логов

Для настройки логов необходимо сделать копию оригинального файла с настройками логов:

- Переходим в папку {karaf}/etc/
- Находим файл *org.ops4j.pax.logging.cfg* и создаем копию;
- Копию оригинала называем *org.ops4j.pax.logging.cfg.orig*

Далее заменяем файл на [этот](docs/org.ops4j.pax.logging.cfg).

# Конфигурация SSE-клиента

Для конфигурации SSE-клиента необходимо перейти в менеджер конфигураций в karaf (в webconsole по адресу `/system/console/configMgr`).

Вид настроек:

![SseClientImage](docs/SseConfig.png)


