package ru.volodin.utils.sse.rest.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.sse.SseEventSink;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@Path("/")
public interface ExternalDataApi {

	@GET
	@Path("/data")
	@Produces({ "text/event-stream" })
	@Operation(summary = "Данные с внешнего сервера", tags = { "External Data" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Данные с внешнего сервера", content = @Content(schema = @Schema(implementation = String.class), examples = @ExampleObject("{\"type\":\"putWire\",\"place\":\"6\",\"len\":267,\"rtime\":1613646766000}"))),
			@ApiResponse(responseCode = "500", description = "Произошла ошибка при получении данных с внешнего сервера", content = @Content(schema = @Schema(implementation = String.class))) })
	public void getExternalDevicesData(@QueryParam("clientId") String clientId, @Context SseEventSink sseEventSink);

}