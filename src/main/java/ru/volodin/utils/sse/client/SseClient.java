package ru.volodin.utils.sse.client;

import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.sse.InboundSseEvent;
import javax.ws.rs.sse.SseEventSource;

import org.apache.cxf.jaxrs.sse.client.SseEventSourceBuilderImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import ru.volodin.utils.sse.config.SseClientConfig;
import ru.volodin.utils.sse.rest.api.ExternalDataApi;

@Component(configurationPid = {"ru.volodin.utils.sse.config.SseClientConfig"})
public class SseClient {

	private Short clientId;
	private String serviceUrl;
	private String methodName;

	private final Logger logger = LogManager.getLogger(this.getClass());

	private Client client;
	private WebTarget target;
	private SseEventSource eventSource;

	private ExecutorService provider = Executors.newSingleThreadExecutor();

	@Activate
	public void activate(SseClientConfig config) {
		logger.info("Активация SseClient компоненты");
		this.clientId = config.clientId();
		this.serviceUrl = config.serviceUrl();
		this.methodName = config.methodName();
		this.eventSource = initSse();
		provider.execute(() -> {
			eventSource.open();
			logger.info("Статус соединения: {}", eventSource.isOpen());
		});
	}

	@Deactivate
	public void deactivate() {
		logger.info("Деактивация SseClient компоненты");
		provider.shutdown();
		Optional.ofNullable(eventSource).ifPresent(consumer -> {
			if (consumer.isOpen())
				consumer.close();
		});
		this.eventSource = null;
		this.target = null;
		this.client = null;
	}

	public void onMessage(InboundSseEvent event) {
		String payload = "";
		try {
			payload = event.readData();
		} catch (ProcessingException e) {
			logger.error("Ошибка при чтении данных из события с идентификатором: {} \n.",
					event.getId(), e);
		}
		logger.info("eventId: {}, eventName: {}, eventData: {}, eventComment: {}", event.getId(),
				event.getName(), payload, event.getComment());
	}

	private SseEventSource initSse() {
		this.client = ClientBuilder.newBuilder().connectTimeout(15, TimeUnit.MINUTES)
				.readTimeout(0, TimeUnit.SECONDS).build();
		this.target = getWebTarget(serviceUrl, clientId, methodName);
		final ClassLoader currentClassLoader = Thread.currentThread().getContextClassLoader();
		Thread.currentThread()
				.setContextClassLoader(SseEventSourceBuilderImpl.class.getClassLoader());
		final SseEventSource source = SseEventSource.target(this.target).build();
		Thread.currentThread().setContextClassLoader(currentClassLoader);
		source.register(this::onMessage, e -> {
			logger.error("Ошибка в SSE, разорвано соединение.", e);
			provider.execute(() -> {
				reconnectEventSource();
				logger.info("Попытка открытия нового соединения");
				source.open();
			});
		}, () -> {
			logger.info("SSE соединение закрыто сервером.");
			provider.execute(() -> {
				reconnectEventSource();
				logger.info("Попытка открытия нового соединения");
				source.open();
			});
		});
		logger.info("Клиент sse запущен.");
		return source;
	}

	private void reconnectEventSource() {
		try {
			if (eventSource.isOpen()) {
				eventSource.close();
				provider.shutdown();
			}
		} catch (final Exception e) {
			logger.error("Ошибка при завершении источника событий", e);
		}
		eventSource = initSse();
		logger.info("Статус соединения: {}", eventSource.isOpen());
	}

	private WebTarget getWebTarget(final String serviceUrl, final short unitId,
			final String methodName) {
		WebTarget webTarget = client.target(serviceUrl)
				.path(UriBuilder.fromMethod(ExternalDataApi.class, methodName).build().toString())
				.queryParam("clientId", unitId);
		logger.info("Созданный web-target: {}", webTarget);
		return webTarget;
	}

}
