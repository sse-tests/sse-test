package ru.volodin.utils.sse.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(pid = "ru.volodin.utils.sse.config.SseClientConfig",
		name = "SseClient config")
public @interface SseClientConfig {

	@AttributeDefinition(name = "Client Id", description = "Идентификатор клиента")
	short clientId() default 0;

	@AttributeDefinition(name = "SSE Service Url",
			description = "URL-адрес сервиса SSE для получения данных")
	String serviceUrl() default "http://localhost:8181/cxf/sse-server-external-data";

	@AttributeDefinition(name = "Method Name",
			description = "Название метода API для получения данных")
	String methodName() default "getExternalDevicesData";

}
