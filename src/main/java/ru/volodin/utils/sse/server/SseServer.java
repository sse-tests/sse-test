package ru.volodin.utils.sse.server;

import javax.ws.rs.core.Context;
import javax.ws.rs.sse.Sse;
import javax.ws.rs.sse.SseEventSink;

import ru.volodin.utils.sse.broadcaster.SseService;
import ru.volodin.utils.sse.rest.api.ExternalDataApi;


public class SseServer implements ExternalDataApi {

	private Sse sse;
	private SseService sseService;

	@Context
	public void setSse(Sse sse) {
		this.sse = sse;
	}

	public void setSseService(SseService sseService) {
		this.sseService = sseService;
	}

	@Override
	public void getExternalDevicesData(String clientId, SseEventSink sseEventSink) {
		sseService.setSse(sse);
		sseService.registerSseEventSink(clientId, sseEventSink);
	}

}
