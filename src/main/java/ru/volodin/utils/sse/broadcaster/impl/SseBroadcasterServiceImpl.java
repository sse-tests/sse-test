package ru.volodin.utils.sse.broadcaster.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.sse.OutboundSseEvent;
import javax.ws.rs.sse.Sse;
import javax.ws.rs.sse.SseBroadcaster;
import javax.ws.rs.sse.SseEventSink;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ru.volodin.utils.sse.broadcaster.BroadcasterService;
import ru.volodin.utils.sse.broadcaster.SseService;

public class SseBroadcasterServiceImpl implements SseService, BroadcasterService {

	private Sse sse;

	private Map<String, SseBroadcaster> sseBroadcasters = new HashMap<>();
	private Map<String, ScheduledExecutorService> providers = new HashMap<>();

	@Override
	public void broadcastExternalData(String clientId, String externalData) {
		SseBroadcaster sseBroadcaster = sseBroadcasters.get(clientId);
		OutboundSseEvent sseEvent =
				this.sse.newEventBuilder().mediaType(MediaType.APPLICATION_JSON_TYPE)
						.id(String.valueOf(System.currentTimeMillis()))
						.data(String.class, externalData).build();
		sseBroadcaster.broadcast(sseEvent);
	}

	@Override
	public void setSse(Sse sse) {
		this.sse = sse;
	}

	@Override
	public void registerSseEventSink(String clientId, SseEventSink sseEventSink) {
		if (!sseBroadcasters.containsKey(clientId))
			sseBroadcasters.put(clientId, sse.newBroadcaster());

		// Передача первичных данных
		sseEventSink.send(this.sse.newEventBuilder().mediaType(MediaType.APPLICATION_JSON_TYPE)
				.data(String.class, generateEventObject(clientId)).build());

		sseBroadcasters.get(clientId).register(sseEventSink);
		ScheduledExecutorService provider = Executors.newSingleThreadScheduledExecutor();
		providers.put(clientId, provider);
		provider.scheduleAtFixedRate(
				() -> broadcastExternalData(clientId, generateEventObject(clientId)), 0L, 1L,
				TimeUnit.MINUTES);
	}

	public void destroy() {
		if (providers != null && !providers.isEmpty()) {
			providers.values().forEach(ScheduledExecutorService::shutdown);
			providers.clear();
		}
		if (sseBroadcasters != null && !sseBroadcasters.isEmpty()) {
			sseBroadcasters.values().forEach(SseBroadcaster::close);
			sseBroadcasters.clear();
		}
	}

	private String generateEventObject(String clientId) {
		final Map<String, Object> map = new HashMap<>();
		map.put("type", "wire");
		map.put("place", clientId);
		map.put("len", 3.50);
		map.put("rtime", System.currentTimeMillis());
		String eventObject;
		try {
			eventObject = new ObjectMapper().writeValueAsString(map);
		} catch (JsonProcessingException e) {
			eventObject = "";
		}
		return eventObject;
	}

}