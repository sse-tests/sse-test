package ru.volodin.utils.sse.broadcaster;

import javax.ws.rs.sse.Sse;
import javax.ws.rs.sse.SseEventSink;

public interface SseService {

	void setSse(Sse sse);

	void registerSseEventSink(String clientId, SseEventSink sseEventSink);

}
