package  ru.volodin.utils.sse.broadcaster;

public interface BroadcasterService {

	void broadcastExternalData(String clientId, String externalData);

}
